import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaveRequestUpdatePage } from "./leave-request-update";

@NgModule({
  declarations: [
    LeaveRequestUpdatePage,
  ],
  imports: [
    IonicPageModule.forChild(LeaveRequestUpdatePage),
  ],
  exports: [
    LeaveRequestUpdatePage
  ]
})
export class FooPageModule {}