import { AuthServiceProvider } from './../../providers/auth-service/auth-service';
import { Component } from '@angular/core';
import { NavController, IonicPage, App, AlertController, LoadingController, Loading } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  user: any;
  employeeCount: any;
  visitorCount: any;
  leave: any;


  loading: Loading;
  constructor(public navCtrl: NavController, private auth: AuthServiceProvider, private app: App, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.user = this.auth.getUserInfo();
    this.employeeCount = { 'count': 0 };
    this.visitorCount = { 'count': 0 };
    this.leave = '';


  }

  public logout() {
    this.auth.logout().subscribe(succ => {
      
      this.app.getRootNav().setRoot('LoginPage');
      
    });
  }

  

  public getEmployeeCount() {
   
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getEmployeeCount().subscribe(data => {
        this.employeeCount = data;
        //this.loading.dismiss();
      }, error => {
       loading.dismiss();
        this.showError('Network Error');
      }, () => {
       
       loading.dismiss();
      });
    });
  }

 

  public getVisitorCount() {
   
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getVisitorCount().subscribe(data => {
        this.visitorCount = data;
       //this.loading.dismiss();
      }, error => {
       loading.dismiss();
        this.showError('Network Error');
      }, () => {
        
       loading.dismiss();
      });
    });
  }

 

  public getNumberOfLeaveToday() {
   
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getNumberOfLeaveToday().subscribe(data => {
       this.leave = data;
       
      }, error => {
         loading.dismiss();
        this.showError('Network Error');
      }, () => {
      
       loading.dismiss();
      });
    });
  }

  

  ngOnInit() {
    if( this.user.role == 'mss_role'){
      this.getEmployeeCount();
      this.getVisitorCount();
      this.getNumberOfLeaveToday()
    }else if(this.user.role == 'ess_role'){
       this.getNumberOfLeaveToday()
    }else{
       this.getEmployeeCount();
      this.getVisitorCount();
     }
  }

  

  

  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

 

  doRefresh(refresher) {
   
    if( this.user.role == 'mss_role'){
      this.getEmployeeCount();
      this.getVisitorCount();
      this.getNumberOfLeaveToday()
    }else if(this.user.role == 'ess_role'){
       this.getNumberOfLeaveToday()
    }else{
       this.getEmployeeCount();
      this.getVisitorCount();
     }
  
      refresher.complete();
   
  }

  ionViewDidLoad() {
    
  }



}
