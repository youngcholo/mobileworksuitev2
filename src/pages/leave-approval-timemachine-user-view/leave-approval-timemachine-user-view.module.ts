import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { FilterModule } from "../filters/filter.module";
import { LeaveApprovalTimemachineUserViewPage } from './leave-approval-timemachine-user-view';


@NgModule({
  declarations: [
    LeaveApprovalTimemachineUserViewPage,
  ],
  imports: [
    IonicPageModule.forChild(LeaveApprovalTimemachineUserViewPage),
    FilterModule,
  ],
  exports: [
    LeaveApprovalTimemachineUserViewPage
  ]
})
export class LeaveApprovalTimemachineUserViewPageModule {}