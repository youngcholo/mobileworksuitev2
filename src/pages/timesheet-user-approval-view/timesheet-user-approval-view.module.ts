import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimesheetUserApprovalViewPage } from './timesheet-user-approval-view';

@NgModule({
    declarations: [
        TimesheetUserApprovalViewPage,
    ],
    imports: [
      IonicPageModule.forChild(TimesheetUserApprovalViewPage),
    ],
    exports: [
        TimesheetUserApprovalViewPage
    ]
  })
  export class TimesheetUserApprovalViewPageModule {}