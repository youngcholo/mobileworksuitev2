import { NgCalendarModule } from 'ionic2-calendar';
import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, Events, AlertController, LoadingController, ActionSheetController, MenuController } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { ApiProvider } from "../../providers/api/api";
import moment from 'moment';
import 'moment-timezone';
import { DatePipe } from '@angular/common';
import { ModalController } from 'ionic-angular';
import { CalendarModal, CalendarModalOptions, DayConfig, CalendarResult } from "ion2-calendar";

/**
 * Generated class for the TimesheetUserCalendarPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-timesheet-user-calendar',
  templateUrl: 'timesheet-user-calendar.html',
})
export class TimesheetUserCalendarPage {
  eventSource;
  viewTitle;
  allDayLabel;
  noEventsLabel;
  startingDayMonth;
  formatDayHeader;

  isToday: boolean;
  calendar = {
    mode: 'month',
    currentDate: new Date(),
    qMode: 'remote',
    dateFormatter: {
      formatMonthViewDay: function (date: Date) {
        return date.getDate().toString();
      },
      formatMonthViewDayHeader: function (date: Date) {
        return 'MonMH';
      },
      formatMonthViewTitle: function (date: Date) {
        return 'testMT';
      },
      formatWeekViewDayHeader: function (date: Date) {
        return 'MonWH';
      },
      formatWeekViewTitle: function (date: Date) {
        return 'testWT';
      },
      formatWeekViewHourColumn: function (date: Date) {
        return 'testWH';
      },
      formatDayViewHourColumn: function (date: Date) {
        return 'testDH';
      },
      formatDayViewTitle: function (date: Date) {
        return 'testDT';
      }
    }
  };
  searchDto: any;
  username: any;
  clipboard: any;
  selectedDate: any;
  tempTitle: any;



  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private events: Events, public actionSheetCtrl: ActionSheetController, public modalCtrl: ModalController) {
    this.clipboard = {};
    this.searchDto = { empId: null };
    this.username = auth.getUserInfo().username;
    this.selectedDate = {};
    this.tempTitle = {};
    this.getEmpId();
    this.allDayLabel = 'Timesheet';
    this.noEventsLabel = 'No Timesheet';
    this.startingDayMonth = 1;
    this.formatDayHeader = 'E'
    this.events.subscribe('reloadCalendar', (data) => {
      this.eventSource = {};
      //console.log("reload calendar");
      //console.log(this.searchDto);
      if (data instanceof Array) {
        this.calendar.currentDate = new Date(data[0].date)
      } else {
        this.calendar.currentDate = new Date(data.date)
      }


      this.loadLeaveEvents(this.searchDto);
      //this.navCtrl.pop();
    });
  }

  ngOnInit() {
    // //console.log("la views ng init loaded");
    this.getEmpId();
  }

  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  getEmpId() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getEmpId(this.username).subscribe(data => {

        this.searchDto.empId = data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  loadLeaveEvents(searchDto) {

   //console.log("load events");
    let loading = this.loadingCtrl.create({
      content: 'Loading Timesheets...'
    });
    loading.present().then(() => {
      this.api.loadTimesheetUserEvents(searchDto).subscribe(data => {
       // console.log(data);

        if (data != null && data.length != 0) {

          data.forEach((o, i, a) => {
            //a[i].startTime = new Date(a[i].startTime);
            //a[i].endTime = new Date(a[i].endTime);
            var d = new Date(a[i].startTime);
            d.setHours(12, 0, 0);
            var d1 = new Date(a[i].endTime)
            d1.setHours(12, 0, 0);
            a[i].startTime = d;
            a[i].endTime = d1;
          });

          this.eventSource = data;
          //console.log(this.eventSource);
          //this.eventSource[0].startTime= new Date(this.eventSource[0].startTime);
          //this.eventSource[0].endTime= new Date(this.eventSource[0].endTime);
        }


        // //console.log(data);
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }



  onViewTitleChanged(title) {
    // //console.log(title);
    this.viewTitle = title;
  }

  onEventSelected(event) {
   
    this.presentActionSheetEvent(event)
    ////console.log('Event selected:' + event.startTime + '-' + event.endTime + ',' + event.title+'-'+event.id);
    //var m = moment(event.startTime); 
    //m.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
    //console.log(m.toISOString());
   //this.getTimesheetMobileDtoOfEvent(e);
  }

  getTimesheetMobileDtoOfEvent(event) {
    // //console.log(JSON.stringify(event));
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getTimesheetUserView(event).subscribe(data => {
       // console.log("data to push :");
       // console.log(data);
        this.navCtrl.push('TimesheetUserViewPage', { timesheetDto: data });
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  eventToClipboard(event) {
    //console.log(JSON.stringify(event));
    let loading = this.loadingCtrl.create({
      content: 'Copying...'
    });
    loading.present().then(() => {
      this.api.getTimesheetUserView(event).subscribe(data => {
        //console.log(data);
        this.trasformTStoClipBoard(data);
        //console.log(this.clipboard);
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  trasformTStoClipBoard(data) {
   
    this.clipboard.breakEnd = new Date(data.breakEnd + (new Date(data.breakEnd).getTimezoneOffset() * 60 * 1000));
    this.clipboard.breakEndCoords = data.breakEndCoords;
    this.clipboard.breakEndLocation = data.breakEndLocation;
    this.clipboard.breakStart = new Date(data.breakStart + (new Date(data.breakStart).getTimezoneOffset() * 60 * 1000));
    this.clipboard.breakStartCoords = data.breakStartCoords;
    this.clipboard.breakStartLocation = data.breakStartLocation;
    this.clipboard.costCentre = data.costCentre;
    this.clipboard.date = new Date(data.date + (new Date(data.date).getTimezoneOffset() * 60 * 1000));
    this.clipboard.endTime = new Date(data.endTime + (new Date(data.endTime).getTimezoneOffset() * 60 * 1000));
    this.clipboard.endTimeCoords = data.endTimeCoords;
    this.clipboard.endTimeLocation = data.endTimeLocation;
    this.clipboard.startTime = new Date(data.startTime + (new Date(data.startTime).getTimezoneOffset() * 60 * 1000));
    this.clipboard.startTimeCoords = data.startTimeCoords;
    this.clipboard.startTimeLocation = data.startTimeLocation;
    this.clipboard.transCode = data.transCode;
    this.clipboard.username = data.username;
   
  }

  newClipBoard(clipboard){
    var newclipboard={
    breakEnd : clipboard.breakEnd,
    breakEndCoords : clipboard.breakEndCoords,
    breakEndLocation : clipboard.breakEndLocation,
    breakStart : clipboard.breakStart,
    breakStartCoords :  clipboard.breakStartCoords,
    breakStartLocation : clipboard.breakStartLocation,
    costCentre :  clipboard.costCentre,
    date :  clipboard.date,
    endTime :  clipboard.endTime,
    endTimeCoords :  clipboard.endTimeCoords,
    endTimeLocation :  clipboard.endTimeLocation,
    startTime : clipboard.startTime,
    startTimeCoords :  clipboard.startTimeCoords,
    startTimeLocation : clipboard.startTimeLocation,
    transCode : clipboard.transCode,
    username : clipboard.username};
    
    return newclipboard;
  }

  persistNewUserTimesheetDto() {
    //console.log(this.clipboard);
    //this.showInfo(JSON.stringify(this.timesheetDto));
    let loading = this.loadingCtrl.create({
      content: 'Saving...'
    });
    loading.present().then(() => {
      this.api.postNewUserTimesheetMobileDto(this.clipboard).subscribe(data => {
        //console.log(data);
        this.showInfo('Timesheet Created');
        this.events.publish('reloadCalendar', data);
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  persistNewUserTimesheetDtos(ldto) {
    //console.log(ldto);
    //this.showInfo(JSON.stringify(this.timesheetDto));
    let loading = this.loadingCtrl.create({
      content: 'Saving...'
    });
    loading.present().then(() => {
      this.api.postNewUserTimesheetMobileDtos(ldto).subscribe(data => {
        //console.log(data);
        this.showInfo('Timesheet(s) Created : ' + data.length);
        this.events.publish('reloadCalendar', data);
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }




  changeMode(mode) {
    this.calendar.mode = mode;
  }

  today() {
    this.calendar.currentDate = new Date();
  }

  onTimeSelected(ev) {
    //console.log('Selected time: ' + ev.selectedTime + ', hasEvents: ' +
    // (ev.events !== undefined && ev.events.length !== 0) + ', disabled: ' + ev.disabled);

    var selecteddate = moment(new Date(ev.selectedTime)).format();
    this.selectedDate = selecteddate;
    if (Object.keys(this.clipboard).length !== 0) {

      //this.presentActionSheetSelectedTime(selecteddate);
    }
  }

  checkClipboard() {
    if (Object.keys(this.clipboard).length !== 0) {
      return true;
    } else {
      return false;
    }
  }

  public updateDateOfTime(oldDate, newDate) {
    var hour = new Date(oldDate).getHours();
    var minute = new Date(oldDate).getMinutes();
    var seconds = new Date(oldDate).getSeconds();
    var date = new Date(newDate);
    date.setHours(hour);
    date.setMinutes(minute);
    date.setSeconds(seconds);
    return date;
  }

  dateChanged(newDate) {

    var startTime = this.updateDateOfTime(this.clipboard.startTime, newDate);
    var endTime = this.updateDateOfTime(this.clipboard.endTime, newDate);
    var breakStart = this.updateDateOfTime(this.clipboard.breakStart, newDate);
    var breakEnd = this.updateDateOfTime(this.clipboard.breakEnd, newDate);
    this.clipboard.startTime = moment(startTime).format();
    this.clipboard.endTime = moment(endTime).format();
    this.clipboard.breakStart = moment(breakStart).format();
    this.clipboard.breakEnd = moment(breakEnd).format();

  }

  onCurrentDateChanged(event: Date) {
    var today = new Date();
    today.setHours(0, 0, 0, 0);
    event.setHours(0, 0, 0, 0);
    this.isToday = today.getTime() === event.getTime();
  }





  onRangeChanged(ev) {
   //console.log('range changed: startTime: ' + ev.startTime + ', endTime: ' + ev.endTime);
    var day = moment(ev.startTime).format('DD/MM/YYYY');
    //console.log(day);
    var day1 = moment(ev.endTime).format('DD/MM/YYYY');
    //console.log(day1);

    let date = ev.startTime.getDate(),
      month = (ev.startTime.getMonth() + (date !== 1 ? 1 : 0)) % 12,
      year = ev.startTime.getFullYear() + (date !== 1 && month === 0 ? 1 : 0),
      headerDate = new Date(year, month, 1);
    headerDate.setHours(12);
    headerDate.setMinutes(0);
    headerDate.setSeconds(0);

    var datePipe = new DatePipe('en-US');
    this.tempTitle = datePipe.transform(headerDate, 'MMMM yyyy');

    this.searchDto.from = day;
    this.searchDto.to = day1;
    //console.log(this.searchDto);
    this.loadLeaveEvents(this.searchDto);
  }



  markDisabled = (date: Date) => {
    var current = new Date();
    current.setHours(0, 0, 0);
    return date < current;
  }

  public presentActionSheetEvent(ev) {
  
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Timesheet',
      buttons: [
        {
          text: 'Copy Timesheet',
          handler: () => {
            var e = { "id": ev.id };
            this.eventToClipboard(e);
          }
        },
        {
          text: 'Details',
          handler: () => {
            var e = { "id": ev.id };
            this.getTimesheetMobileDtoOfEvent(e)
          }
        },
        {
          text: 'Close',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  /*public presentActionSheetSelectedTime(newDate) {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Timesheet',
      buttons: [
        {
          text: 'Paste Timesheet',
          handler: () => {
            this.dateChanged(newDate);
            //console.log(this.clipboard);
            this.persistNewUserTimesheetDto();
          }
        },
        {
          text: 'Clear Clipboard',
          handler: () => {
            this.clipboard = {};
          }
        },

        {
          text: 'Close',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }*/


  ionViewDidLoad() {
    //console.log('ionViewDidLoad TimesheetUserCalendarPage');
  }


  clearClipBoard() {
    this.clipboard = {};
  }

  pasteTS() {
    //console.log(this.selectedDate);
    this.dateChanged(this.selectedDate);
    this.persistNewUserTimesheetDto();
  }

  openRangeCalendar() {
    const options: CalendarModalOptions = {
      pickMode: 'range',
      title: 'RANGE',
      from: new Date(2017, 0),
      defaultScrollTo: new Date(),
      color: 'dark'
    };

    let myCalendar = this.modalCtrl.create(CalendarModal, {
      options: options
    });

    myCalendar.present();

    myCalendar.onDidDismiss((date: { from: CalendarResult; to: CalendarResult }, type: string) => {
      //console.log(date.from);
      //console.log(date.to);
      var listofdto = [];
      if (date != null) {
        var d = new Date(date.from.time);
        var d2 = new Date(date.to.time);
        while (d <= d2) {
         // console.log(d);
          this.dateChanged(d);
          listofdto.push(this.newClipBoard(this.clipboard));
          d.setDate(d.getDate() + 1);
        }

        if (listofdto.length != 0) {
          //console.log(listofdto);
          //send request
          this.persistNewUserTimesheetDtos(listofdto);
        }
      }
    });
  }

  openMultiDateCalendar() {
    const options = {
      pickMode: 'multi',
      title: 'MULTI',
      from: new Date(2017, 0),
      defaultScrollTo: new Date(),
      color: 'dark'
    };

    let myCalendar = this.modalCtrl.create(CalendarModal, {
      options: options
    });

    myCalendar.present();

    myCalendar.onDidDismiss((date: CalendarResult[], type: string) => {
      var listofdto = [];
      if (date != null) {
        for (var i of date) {
         
          var newdate = new Date(i.time);
          //console.log(newdate);
          this.dateChanged(newdate);
          listofdto.push(this.newClipBoard(this.clipboard));
        }

        if (listofdto.length != 0) {
          //console.log(listofdto);
          //send request
          this.persistNewUserTimesheetDtos(listofdto);
        }
      }
    })
  }


}
