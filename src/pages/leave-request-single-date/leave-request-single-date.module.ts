import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaveRequestSingleDatePage } from "./leave-request-single-date";




@NgModule({
  declarations: [
    LeaveRequestSingleDatePage,
   
  ],
  imports: [
    IonicPageModule.forChild(LeaveRequestSingleDatePage),
  ],
  exports: [
    LeaveRequestSingleDatePage
  ]
})
export class LeaveRequestSingleDatePageModule {}