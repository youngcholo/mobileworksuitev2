import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimesheetViewPage } from './timesheet-view';
import { FilterModule } from '../filters/filter.module';


@NgModule({
  declarations: [
    TimesheetViewPage,
  ],
  imports: [
    IonicPageModule.forChild(TimesheetViewPage),
    FilterModule,
  ],
  exports: [
    TimesheetViewPage
  ]
})
export class TimesheetViewPageModule {}