import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimesheetAddPage } from "./timesheet-add";


@NgModule({
  declarations: [
    TimesheetAddPage,
  ],
  imports: [
    IonicPageModule.forChild(TimesheetAddPage),
  ],
  exports: [
    TimesheetAddPage
  ]
})
export class TimesheetAddPageModule {}