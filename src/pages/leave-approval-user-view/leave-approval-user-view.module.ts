import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LeaveApprovalUserViewPage } from "./leave-approval-user-view";
import { FilterModule } from "../filters/filter.module";


@NgModule({
  declarations: [
    LeaveApprovalUserViewPage,
  ],
  imports: [
    IonicPageModule.forChild(LeaveApprovalUserViewPage),
    FilterModule,
  ],
  exports: [
    LeaveApprovalUserViewPage
  ]
})
export class LeaveApprovalUserViewPageModule {}