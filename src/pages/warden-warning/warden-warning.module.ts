import { FilterModule } from './../filters/filter.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WardenWarningPage } from './warden-warning';



@NgModule({
  declarations: [
    WardenWarningPage,
  
    
  ],
  imports: [
    IonicPageModule.forChild(WardenWarningPage),
    FilterModule
  ],
  exports: [
    WardenWarningPage
  ]
})
export class WardenWarningPageModule {}