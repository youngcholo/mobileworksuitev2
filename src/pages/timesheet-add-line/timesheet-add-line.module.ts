import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TimesheetAddLinePage } from "./timesheet-add-line";


@NgModule({
  declarations: [
    TimesheetAddLinePage,
  ],
  imports: [
    IonicPageModule.forChild(TimesheetAddLinePage),
  ],
  exports: [
    TimesheetAddLinePage
  ]
})
export class TimesheetAddLinePageModule {}