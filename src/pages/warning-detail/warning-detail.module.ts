import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WarningDetailPage } from './warning-detail';

@NgModule({
  declarations: [
    WarningDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(WarningDetailPage),
  ],
  exports: [
    WarningDetailPage
  ]
})
export class WarningDetailPageModule {}