import { CallNumber } from '@ionic-native/call-number';
import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

/**
 * Generated class for the RollcallVisitorDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-rollcall-visitor-detail',
  templateUrl: 'rollcall-visitor-detail.html',
})
export class RollcallVisitorDetailPage {
  visitor:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public callNumber :CallNumber ) {
    this.visitor = this.navParams.get('visitor');
  }

  call(number:string){
   
    this.callNumber.callNumber(number, true)
    .then(() => console.log('Launched dialer!'))
    .catch(() => console.log('Error launching dialer'));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RollcallVisitorDetailPage');
  }

}
