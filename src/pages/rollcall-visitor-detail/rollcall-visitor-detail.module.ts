import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RollcallVisitorDetailPage } from "./rollcall-visitor-detail";


@NgModule({
  declarations: [
    RollcallVisitorDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(RollcallVisitorDetailPage),
  ],
  exports: [
    RollcallVisitorDetailPage
  ]
})
export class RollcallVisitorDetailPageModule {}