import { FilterModule } from './../filters/filter.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VisitorRollCallPage } from './visitor-roll-call';
import { StatusFilterPipe } from "../../pipes/status-filter/status-filter";

@NgModule({
  declarations: [
    VisitorRollCallPage,
    
  ],
  imports: [
    IonicPageModule.forChild(VisitorRollCallPage),
    FilterModule
  ],
  exports: [
    VisitorRollCallPage
  ]
})
export class VisitorRollCallPageModule {}