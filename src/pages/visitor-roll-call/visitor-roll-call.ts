import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController, AlertController } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { ApiProvider } from "../../providers/api/api";

/**
 * Generated class for the VisitorRollCallPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-visitor-roll-call',
  templateUrl: 'visitor-roll-call.html',
})
export class VisitorRollCallPage {

  locationsCodes: any;
  timesheetSearchDto: any;
  filterStatus: string;
  visitor: any;
  minlength: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController) {
    this.filterStatus = 'All';
    this.visitor = { wardenRollcallDtos: null };
    this.minlength = 5;
  }



  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }
  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  public loadLocationCode() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getLocation().subscribe(data => {
        ;
        this.locationsCodes = data;
       
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  ngOnInit() {
    this.timesheetSearchDto = { employeeLocationCode: null };
    this.loadLocationCode();
  }

  public getRollcall(searchDto) {


    if (searchDto.employeeLocationCode === null) {
      this.showError('Please select Location!');
    } else {
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present().then(() => {
        this.api.getVisitorRollcall(searchDto).subscribe(data => {
          if (typeof data === 'string' || data instanceof String) {
            this.showError(data);
          } else {
           
            this.visitor.wardenRollcallDtos = data;
          }

        }, error => {
          loading.dismiss();
          this.showError('Network Error');
        }, () => {

          loading.dismiss();
        });
      });

    }

  }

  doRefresh(refresher, searchDto) {
    if (searchDto.employeeLocationCode === null) {
      this.showError('Please select Location!');
    } else {
      let loading = this.loadingCtrl.create({
        content: 'Please wait...'
      });
      loading.present().then(() => {
        this.api.getVisitorRollcall(searchDto).subscribe(data => {
          if (typeof data === 'string' || data instanceof String) {
            this.showError(data);
          } else {
          
            this.visitor.wardenRollcallDtos = data;
          }

        }, error => {
          loading.dismiss();
          this.showError('Network Error');
        }, () => {

          loading.dismiss();
        });
      });

    }
    refresher.complete();

  }

  loadMore(infiniteScroll) {
   
    if (!this.visitor.wardenRollcallDtos) {
      infiniteScroll.complete();
      return;
    }
    
    if (this.minlength < this.visitor.wardenRollcallDtos.length)
      this.minlength += 5;
    infiniteScroll.complete();
  }


  showDetails(visitor) {
   

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getRollcallVisitorDtl(visitor).subscribe(data => {
        this.navCtrl.push('RollcallVisitorDetailPage', { visitor: data });
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });

  }

  persistWarden(wardenRollcallDto) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.persistVisitorWarden(wardenRollcallDto).subscribe(data => {
        Object.assign(data, wardenRollcallDto);
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  updateWarden(wardenRollcallDto) {
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.updateVisitorWarden(wardenRollcallDto).subscribe(data => {
        Object.assign(data, wardenRollcallDto);
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  persistEvac(wardenRollcallDto) {
   
    wardenRollcallDto.status = "Evacuated";
    this.persistWarden(wardenRollcallDto);
  }

  persistMissing(wardenRollcallDto) {
   
    wardenRollcallDto.status = "Missing";
    this.persistWarden(wardenRollcallDto);
  }

  updateToEvac(wardenRollcallDto) {
  
    wardenRollcallDto.status = "Evacuated";
    this.updateWarden(wardenRollcallDto);
  }

  updateToMissing(wardenRollcallDto) {
   
    wardenRollcallDto.status = "Missing";
    this.updateWarden(wardenRollcallDto);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VisitorRollCallPage');
  }

}
