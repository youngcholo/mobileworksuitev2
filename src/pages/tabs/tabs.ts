import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";

/**
 * Generated class for the TabsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tab1 = 'HomePage';
  tab2 = 'LeaveRequestPage';
  tab3 = 'LeaveApprovalPage';
  tab4 = 'TeamCalendarPage';
  tab5 = 'EvacuationRollCallPage';
  tab6 = 'VisitorRollCallPage';
  tab7 = 'WardenWarningPage';
  //tab8 = 'PhotoUploadTestPage';
  tab8 = 'TimesheetPage';
  tab9 = 'LeaveApprovalTimemachinePage';
  tab10='MyDocumentPage';
  user :any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private auth:AuthServiceProvider) {
  this.user = this.auth.getUserInfo();
  //console.log('TABS conctructor called');
  }

  ionViewDidLoad() {
   
    //console.log('ionViewDidLoad TabsPage');
  }

  ionViewCanEnter(){
   
    //console.log('ionViewCanEnter TabsPage');
  }

}
