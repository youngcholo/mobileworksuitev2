import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage, LoadingController, AlertController, Events } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";

/**
 * Generated class for the LeaveRequestViewPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-leave-request-view',
  templateUrl: 'leave-request-view.html',
})

export class LeaveRequestViewPage {
  searchDto: any;
  filterStatus: String;
  userLeaveRequests: any;
  username: any;
  minlength: number;

  constructor(public navCtrl: NavController, public navParams: NavParams, private auth: AuthServiceProvider, private api: ApiProvider, private alertCtrl: AlertController, private loadingCtrl: LoadingController,public events :Events) {
    this.searchDto = { empId: null };
    this.username = auth.getUserInfo().username;
    this.filterStatus = 'All';
    this.minlength = 5;
    this.userLeaveRequests={list:null};
    this.events.subscribe('reloadLRView',(data)=>{
    //console.log('events triggered');
   
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getLeaveRequestGroupViewDtoofUpdatedItem(data.recid).subscribe(res => {
       
       // this.showInfo(JSON.stringify( res.dto));
        
        this.showUpdatedItem(res.dto);
       
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
   });
    
  }


  findIndexToUpdate(newItem) { 
    return newItem.idShift === this;
  }

  showUpdatedItem(newItem){
    let updateItem = this.userLeaveRequests.list.find(this.findIndexToUpdate, newItem.idShift);

    let index = this.userLeaveRequests.list.indexOf(updateItem);


    this.userLeaveRequests.list[index] = newItem;

  }
  
  showError(text) {

    let alert = this.alertCtrl.create({
      title: 'Fail',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  showInfo(text) {

    let alert = this.alertCtrl.create({
      title: 'Info',
      subTitle: text,
      buttons: ['OK']
    });
    alert.present(prompt);
  }

  getEmpId() {

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getEmpId(this.username).subscribe(data => {
       
        this.searchDto.empId= data;
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  public showUserGroupLeaveRequest(searchDto) {
    searchDto.firstResult = 0;

  
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getUserGroupLeaveRequest(searchDto).subscribe(data => {
      
        this.userLeaveRequests = data;
      
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });

  }

  public loadAdditonalLeaveRequest(searchDto) {
 //console.log(this.userLeaveRequests.list.length);
    searchDto.firstResult = this.userLeaveRequests.list.length;

    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getUserGroupLeaveRequest(searchDto).subscribe(data => {

        if (data.list !== null) {
          this.showInfo('Loading  complete.. Please scroll down. ');
          this.userLeaveRequests.list = this.userLeaveRequests.list.concat(data.list);
        } else {
          this.showInfo('No more to load');
        }


      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();

      });
    });
  }

  doRefresh(refresher, searchDto) {
    searchDto.firstResult = 0;


    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.getUserGroupLeaveRequest(searchDto).subscribe(data => {
        this.userLeaveRequests = data;

      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });


    refresher.complete();

  }

  

  loadMore(infiniteScroll) {
    //console.log('min len is ' + this.minlength);
    if (!this.userLeaveRequests.list) {
      infiniteScroll.complete();
      return;
    }

    if (this.minlength < this.userLeaveRequests.list.length)
      this.minlength += 5;
    infiniteScroll.complete();
  }

  showLeaveRequestDtlGroupSingle(leaveRequestViewDto) {
  
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
    
      this.api.getUserLeaveRequestDtoByGroup(leaveRequestViewDto).subscribe(data => {
     
       this.navCtrl.push('LeaveRequestUpdatePage', { newShift: data.dto });
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  showLeaveRequestDtlGroupMulti(leaveRequestViewDto) {
  
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
     
      this.api.getUserLeaveRequestDtoByGroup(leaveRequestViewDto).subscribe(data => {
   
       data.dto.dateRangeFrom= leaveRequestViewDto.startDate;
       data.dto.dateRangTo=leaveRequestViewDto.endDate;
     
       this.navCtrl.push('LeaveRequestUpdatePage', { newShift: data.dto });
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  deleteLeaveRequestDtl(idx,leaveRequestViewDto) {
    //console.log("delete dtl");
    let loading = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loading.present().then(() => {
      this.api.deleteUserLeaveRequestByRecurId(leaveRequestViewDto).subscribe(data => {
     
       this.userLeaveRequests.list.splice(idx,1);
      
      }, error => {
        loading.dismiss();
        this.showError('Network Error');
      }, () => {

        loading.dismiss();
      });
    });
  }

  ngOnInit() {
    //console.log("lr views ng init loaded");
    this.getEmpId();
  }
  
  ionViewDidLoad() {
    //console.log('ionViewDidLoad LeaveRequestViewPage');
  }

  

}
