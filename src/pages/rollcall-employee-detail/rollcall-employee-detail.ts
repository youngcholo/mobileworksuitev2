import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';
import { CallNumber } from "@ionic-native/call-number";

/**
 * Generated class for the RollcallEmployeeDetailPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-rollcall-employee-detail',
  templateUrl: 'rollcall-employee-detail.html',
})
export class RollcallEmployeeDetailPage {
  employee:any;
  
    constructor(public navCtrl: NavController, public navParams: NavParams, public callNumber:CallNumber) {
      this.employee = this.navParams.get('employee');
     
    }
  
    ionViewDidLoad() {
      console.log('ionViewDidLoad WarningDetailPage');
    }
  
    call(number:string){
    
      this.callNumber.callNumber(number, true)
      .then(() => console.log('Launched dialer!'))
      .catch(() => console.log('Error launching dialer'));
    }
  
  }
  