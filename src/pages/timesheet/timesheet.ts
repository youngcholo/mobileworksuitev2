import { Component } from '@angular/core';
import { NavController, NavParams, IonicPage } from 'ionic-angular';

/**
 * Generated class for the TimesheetPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-timesheet',
  templateUrl: 'timesheet.html',
})
export class TimesheetPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad TimesheetPage');
  }

  goToTimesheetAddPage(){
  this.navCtrl.push('TimesheetAddPage');
  }

  goToTimesheetUserCalendarPage(){
    this.navCtrl.push('TimesheetUserCalendarPage');
  }

}
