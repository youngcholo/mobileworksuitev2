import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the LeaveFilterPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'leavefilter',
  pure:false
})
export class LeaveFilterPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   <p *ngIf="userLeaveRequest.status===true" ion-text color="secondary">Status: Approved</p>
        <p *ngIf="userLeaveRequest.status===false && userLeaveRequest.rejected===false" ion-text color="energized">Status: Pending</p>
        <p *ngIf="userLeaveRequest.status===false && userLeaveRequest.rejected===true" ion-text color="danger">Status: Rejected</p>
  call this filter :
        leavefilter:{rejected:filterStatus}
        */
  
  /*transform(items: Array<any>, conditions: { [field: string]: any }): Array<any> {
   
    if (items == null || items.length == 0) {
      return null;
    } else {
      return items.filter(item => {
        for (let field in conditions) {
         
          

          if (conditions[field] == 'All') {
          
            return true;
          }
          else if (conditions[field] == 'Pending') {
             
            if(item['status']===false && item['rejected']===false){
                return true;
              }
          }
          else if (conditions[field] == 'Approved') {
            
            if(item['status']==true){
            return true;
           }
          }
          else if (conditions[field] == 'Rejected') {
            
            if(item['status']===false && item['rejected']===true){
            return true;
          }
          }
          else{
            return false
          }
        }
        //return true;
      });

    }

  }*/

  transform(items: Array<any>, conditions: {[field: string]: any}): Array<any> {
    
if(items==null){
        return items;
    }else{
        return items.filter(item => {
        for (let field in conditions) {
          
            if(field=='warning' && conditions[field]=='All'){
                 return true;
            }
            else if (item[field] !== conditions[field]) {
                return false;
            }
        }
        return true;
    });

    }
    
}

}